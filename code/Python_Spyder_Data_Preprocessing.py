### [DOCUMENTATIE]
### Dit is een Python script geschreven in IDE Spyder
### Er is gekozen om data preprocessing van de dataset van Rijkswaterstaat in Spyder script te doen
### Dit is omdat het bestand te groot is (2GB, meer dan 2 miljoen regels) voor data preprocessing in Jupyter Notebook
### Het duurt heel lang om bewerkingen uit te voeren in Jupyter Notebook 
### Het script zorgt voor het wegfilteren van niet relevante kolommen en het hernoemen van kolommen 

### Importeren van packages
import pandas as pd
import numpy as np
import os

### Instellen van working directory
os.chdir('C:/Users/Andre/Documents/ITvitae/Studie/Data Science (Anchormen)/Module B/Projectopdracht/Project-Rijkswaterstaat-Andre/data/waterkwaliteit_data')

### Inladen van dataset
RWS_data = pd.read_csv("data_tot.csv", encoding="latin1", index_col=0)

### DATA PREPROCESSING
### Wegfilteren van niet relevante kolommen (69 kolommen)
RWS_data = RWS_data.drop(['TIJD',
                        'DOM',
                        'WNS',
                        'BEW',
                        'PLT:REFVLAK',
                        'PLT:BMH',
                        'SGK',
                        'ORG',
                        'IVS',
                        'LOC:TYPE',
                        'OGI',
                        'ANI',
                        'BHI',
                        'BMI',
                        'SYS',
                        'TYP',
                        'TYD:BEGINDAT',
                        'TYD:BEGINTYD',
                        'TYD:EINDDAT',
                        'TYD:EINDTYD',
                        'STA:BEGINDAT',
                        'STA:BEGINTYD',
                        'STA:EINDTYD',
                        'EXTCODE',
                        'BRON',
                        'DATUMTIJDWAARDE',
                        'WNSOMS',
                        'BEWOMS',
                        'SGKOMS',
                        'ORGOMS',
                        'IVSOMS',
                        'OGIOMS',
                        'ANIOMS',
                        'BHIOMS',
                        'BMIOMS',
                        'VATOMS',
                        'SYSOMS',
                        'TYPOMS',
                        'ID',
                        'FHXH',
                        'FHN',
                        'FHNH',
                        'FXX',
                        'FXXH',
                        'TN',
                        'TNH',
                        'TX',
                        'TXH',
                        'T10N',
                        'T10NH',
                        'SQ',
                        'SP',
                        'Q',
                        'DR',
                        'RHX',
                        'RHXH',
                        'EV24',
                        'PX',
                        'PXH',
                        'PN',
                        'PNH',
                        'VVN',
                        'VVNH',
                        'VVX',
                        'VVXH',
                        'UX',
                        'UXH',
                        'UN',
                        'UNH'], axis=1)

### Hernoemen van kolommen (48 kolommen)
RWS_data = RWS_data.rename(columns={'knmi_STN':'knmi_station',
                                    'DATUM':'datum',
                                    'LOC':'locatie',
                                    'PAR':'parameter',
                                    'BGC':'rapportage_grens',
                                    'WAARDE':'meetwaarde',
                                    'KWC':'kwaliteits_keurmerk',
                                    'EHD':'eenheid',
                                    'HDH':'hoedanigheid',
                                    'ANA':'analyse_methode',
                                    'CPM':'compartiments_code',
                                    'BEM':'bemonsterings_methode',
                                    'BTX':'biotaxonomie',
                                    'BTN':'btn',
                                    'GBD':'gebieds_code',
                                    'LOC:COORDSRT':'locatie_coordinaten_soort',
                                    'LOCOMS':'locatie_omschrijving',
                                    'PAROMS':'parameter_omschrijving',
                                    'EHDOMS':'eenheid_omschrijving',
                                    'HDHOMS':'hoedanigheid_omschrijving',
                                    'ANAOMS':'analyse_methode_omschrijving',
                                    'CPMOMS':'compartiments_code_omschrijving',
                                    'BEMOMS':'bemonsterings_methode_omschrijving',
                                    'BTXCOD':'biotaxonomie_code',
                                    'BTXOMS':'biotaxonomie_omschrijving',
                                    'BTNOMS':'btn_omschrijving',
                                    'GBDOMS':'gebieds_code_omschrijving',
                                    'LOC:X':'locatie_X',
                                    'LOC:Y':'locatie_Y',
                                    'PLT:X':'plt_X',
                                    'PLT:Y':'plt_Y',
                                    'VAT':'vat',
                                    'STA:RKSSTATUS':'invoer_definitief_of_niet',
                                    'is_PAK':'is_PAK',
                                    'X_RD':'X_RD',
                                    'Y_RD':'Y_RD',
                                    'X_WGS':'X_WGS',
                                    'Y_WGS':'Y_WGS',
                                    'YYYYMMDD':'knmi_datum',
                                    'DDVEC':'vector_mean_wind_direction_in_degrees',
                                    'FHVEC':'vector_mean_windspeed ',
                                    'FG':'daily_mean_windspeed',
                                    'FHX':'maximum_hourly_mean_windspeed',
                                    'TG':'daily_mean_temperature',
                                    'RH':'daily_precipitation_amount',
                                    'PG':'daily_mean_sea_level_pressure',
                                    'NG':'mean_daily_cloud_cover',
                                    'UG':'daily_mean_relative_atmospheric_humidity'})

### De bewerkte data opslaan als csv bestand
RWS_data.to_csv('data_gefilterd(NIET_AANPASSEN).csv')

