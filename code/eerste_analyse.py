# -*- coding: utf-8 -*-
# *** Spyder Python Console History Log ***

## ---(Sun Apr 22 06:25:15 2018)---
import pandas as pd
import numpy as np
import os
import seaborn as sns

os.chdir('D:/andrejangjenna/project-rws/data/')

RWS_data = pd.read_csv("data_gefilterd(NIET_AANPASSEN).csv",encoding="latin1", index_col=0)
chlorof_data=RWS_data.loc[RWS_data['parameter']=='CHLFa',:].loc[:,['locatie','meetwaarde']]
df=chlorof_data.loc[:,['locatie','meetwaarde']].groupby('locatie')
chlorof_meetw=df['meetwaarde'].agg('max')

chlorof_data=RWS_data.loc[RWS_data['parameter']=='NO3',:].loc[:,['locatie','meetwaarde']]
df=chlorof_data.loc[:,['locatie','meetwaarde']].groupby('locatie')
nitraat_meetw=df['meetwaarde'].agg('max')

chlorof_data=RWS_data.loc[RWS_data['parameter']=='PO4',:].loc[:,['locatie','meetwaarde']]
df=chlorof_data.loc[:,['locatie','meetwaarde']].groupby('locatie')
fosfaat_meetw=df['meetwaarde'].agg('max')

nitraat_fosfaat=pd.concat([nitraat_meetw,fosfaat_meetw],axis=1)
nitraat_fosfaat.columns=(['nitraat','fosfaat'])
nitraat_fosfaat['nitraat'].corr(nitraat_fosfaat['fosfaat'])

chlorof_nitr=pd.concat([chlorof_meetw,nitraat_meetw.iloc[1:103]],axis=1)
chlorof_nitr.columns=(['chlorof','nitraat'])
chlorof_nitr['chlorof'].corr(chlorof_nitr['nitraat'])

chlorof_fosfaat=pd.concat([chlorof_meetw,fosfaat_meetw.iloc[1:103]],axis=1)
chlorof_fosfaat.columns=(['chlorof','fosfaat'])
chlorof_fosfaat['chlorof'].corr(chlorof_fosfaat['fosfaat'])
